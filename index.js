function countLetter(letter, sentence) {
    let result = 0;
    if(letter.length == 1 && (/[a-zA-Z]/).test(letter)){ //studied regex for capstone 2 validations
        for (let i = 0 ; i < sentence.length ; i++) 
            result = (sentence[i] == letter) ? ++result : result;
        return result;
    }
    else
        return undefined;
}

function isIsogram(text) {
    text = text.toLowerCase();
    while(text.length > 0) {
        let length = text.length;
        text = Array.prototype.filter.call(text, (x) => {return (x != text[length-1])});
        if (length - text.length > 1)
            return false;
    }
    return true;
}

function purchase(age, price) {
    if(age < 13)
        return undefined;
    else if((age >= 13 && age <=21) || age > 64) 
        return((price - (price * 0.2)).toFixed(2).toString());
    else
        return((price).toFixed(2).toString());
}

function findHotCategories(items) {
    let result = [];
    for(let i = 0; i < items.length; i++) 
        if (items[i].stocks == 0)
            result.push(items[i].category);
    return [...new Set(result)]; // I admit, I got this idea from the internet, though of course,
                                 // I had to understand first if this is better. I am aware of set and the spread operator.
}

function findFlyingVoters(candidateA, candidateB) {
    return (candidateA.filter(x => {return(candidateB.includes(x))}));
}

    //opo ang tamad ko sa code. self-study lang kasi ako dati and napagdaanan ko na yung grabeng nested for loops,
    //then realized meron namang easier ways to get results.

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};